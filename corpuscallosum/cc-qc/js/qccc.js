var data=new Array();
var index=127;
var currentSubjectId;

loadData();
function loadData()
{
    console.log("Load data");

    // load data
    $.get("qc.txt", function(thedata)
    {
        var arr=thedata.split("\n");
        $("#date").html("Last version: "+arr[0]);
        for(i=1;i<arr.length;i++)
        {
            var arr1=arr[i].split(";");
            if(arr1[0])
            {
                data.push(arr1[0]);
                $("table#qctable").append(   "<tr id=\""+(i-1)+"\">"+
                                        "<td id=\""+(i-1)+"-0\">"+arr1[0]+"</td>"+
                                        "<td contenteditable=\"true\" id=\""+(i-1)+"-1\">"+arr1[1]+"</td>"+
                                        "<td contenteditable=\"true\" id=\""+(i-1)+"-2\">"+arr1[2]+"</td></tr>");
                if(i==1)
                {
                    currentSubjectId=arr1[0];
                    doImageDisplay();
                }
            }
        }
        
		/*
		$("table#qctable tr").click(function() {
			$("td.td-selected").removeClass('td-selected');
			$(this).children('td').addClass('td-selected');
		});
		*/
        
        console.log("data loaded");

		// configure display image if focus  
		$("td[contenteditable]").focus(function ()
		{
			var j=$(this).parent().attr('id');
			currentSubjectId=data[j];
			doImageDisplay();
			$('.td-selected').removeClass('td-selected');
			$(this).parent().children('td').addClass('td-selected');
		});

		// configure keyboard navigation
		$("td[contenteditable]").keydown(function(e)
		{
			var pos=$(this).attr('id').split("-");
			y=parseInt(pos[0]);
			x=parseInt(pos[1]);
			if(e.which==38)
				$("#"+((y-1+data.length)%data.length)+"-"+x).focus();
			else
			if(e.which==13||e.which==40)	// enter or tab
			{
				e.preventDefault();
				$("#"+((y+1)%data.length)+"-"+x).focus();
			}
			
			return true;
		});
    });
}
function doImageDisplay()
{
    var sub=currentSubjectId;
    var i=index;
    
    $('img#one').attr('src',"data/"+sub+".aseg."+i+".jpg");
    $('img#one').attr('onmouseover','this.src="data/'+sub+'.orig.'+i+'.jpg"');
    $('img#one').attr('onmouseout','this.src="data/'+sub+'.aseg.'+i+'.jpg"');
    $('img#two').attr('src',"data/"+sub+".aseg."+(i+1)+".jpg");
    $('img#two').attr('onmouseover','this.src="data/'+sub+'.orig.'+(i+1)+'.jpg"');
    $('img#two').attr('onmouseout','this.src="data/'+sub+'.aseg.'+(i+1)+'.jpg"');
    $('img#three').attr('src',"data/"+sub+".aseg."+(i+2)+".jpg");
    $('img#three').attr('onmouseover','this.src="data/'+sub+'.orig.'+(i+2)+'.jpg"');
    $('img#three').attr('onmouseout','this.src="data/'+sub+'.aseg.'+(i+2)+'.jpg"');
    $('img#four').attr('src',"data/"+sub+".aseg."+(i+2)+".jpg");
    $('img#four').attr('onmouseover','this.src="data/'+sub+'.orig.'+(i+3)+'.jpg"');
    $('img#four').attr('onmouseout','this.src="data/'+sub+'.aseg.'+(i+3)+'.jpg"');
    $('#SubID').html("Subject ID: "+sub);

    $("div#one").html("<b>"+i+"</b>");
    $("div#two").html("<b>"+(i+1)+"</b>");
    $("div#three").html("<b>"+(i+2)+"</b>");
    $("div#four").html("<b>"+(i+3)+"</b>");
}
function previous()
{
    index--;
    if(index<125)
        index=125;
    else
        doImageDisplay();
}
function next()
{
    index++;
    if(index>128)
        index=128;
    else
        doImageDisplay();
}
function revertQC()
{
    $.get("loadqc.php", function(thedata)
    {
        var arr=thedata.split("\n");
        $("#date").html("Last version: "+arr[0]);
        for(i=1;i<arr.length;i++)
        {
            var arr1=arr[i].split(";");
            $("#"+(i-1)+"-0").text(arr1[0]);
            $("#"+(i-1)+"-1").text(arr1[1]);
            $("#"+(i-1)+"-2").text(arr1[2]);
        }
    });
}
function saveQC()
{
    if(MyLoginWidget.loggedin==false)
    {
    	alert("You have to log in to be able to save\n");
    	return;
    }
    var now = new Date();
    var date=now.toString("yyyy-MM-dd HH:mm:ss");
    var name="'"+($("#saveQC").val()||"Untitled")+"' saved on "+date+" by "+MyLoginWidget.username;
    var	string=name+"\n";
    for(i=0;i<data.length;i++)
        string=string+data[i]+";"+$("#"+i+"-1").text()+";"+$("#"+i+"-2").text()+"\n"

    $.ajax({    type: "POST",
                url: "saveqc.php",
                data: { qc: string },
                error: function(jqXHR, textStatus, errorThrown){ console.log("ERROR: "+textStatus);},
                success:function(data, textStatus, jqXHR){console.log("SUCCESS: "+textStatus);}
                }).done(function( msg ){
                            console.log( "MESSAGE: " + msg );
                            $("#date").html(name);
                        });
}
/*
function revertQC()
{
    alert('Revert')
}
*/
